;;; delete-region.el -- makes emacs delete a shift-marked region when hitting backspace

;; This fixes a problem with the shift mark in emacs. Without this fix, emacs will not delete the marked region, which annoyed me.
;; special care needed to be taken to allow special handling of backspace within slime, and within the C-s search buffer.

;; use in .emacs with:
;; (global-set-key [backspace] 'tweakemacs-backward-delete-region-or-char)
;; (define-key isearch-mode-map [backspace] 'isearch-delete-char)


(defun requires-arg (arglist)
  (when arglist
    (and (not (eql '&optional (car arglist)))
         (not (eql '&rest (car arglist))))))

(defun requires-arg-fun (fun-symbol)
  (let ((fun (symbol-function fun-symbol)))
    (ecase (type-of fun)
      (cons (requires-arg (second fun)))
      (compiled-function (requires-arg (aref fun 0)))
      (subr (eql 0 (subr-arity fun))))))

(defun tweakemacs-backward-delete-region-or-char (&optional default-fun)
  "Backward delete a region or a single character with textmate."
  (interactive)
  (let ((fn (or
             default-fun
             (cdar (minor-mode-key-binding [127]))
             (cdar (minor-mode-key-binding [backspace])))))
    (if mark-active
        (kill-region (region-beginning) (region-end))
      (if (and fn (not (equal fn 'tweakemacs-backward-delete-region-or-char)))
            (condition-case nil
                (if (requires-arg-fun fn)
                    (funcall fn 1) ;; assume requires just one arg
                    (funcall fn))
              (error (funcall fn 1)))
        (backward-delete-char 1)))))



(provide 'delete-region-backspace)

;;; end of delete-region-backspace.el

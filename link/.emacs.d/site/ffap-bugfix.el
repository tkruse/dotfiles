;;; ffap-bugfix.el --- fixes undesired behavior in ffap (find-file-at-point) also used in ido-mode

;;
;; Copyright (C) 2010 Thibault Kruse
;;
;; Author: Thibault Kruse
;; Keywords: ffap, convenience, bugfix
;; Version: 0.1
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;; 02111-1307, USA.
;;
;; Commentary
;; In emacs 23.1, ido-find-file or ffap offer useless guesses when point is at:
;; /*****|
;; or
;; /**|
;; or
;; </somenode|  (though strangely not with any tag?)
;; <somenode />|
;;
;; this advice wraps around ffap-file-at-point preventing this behavior.
;;
;; use in .emacs.d:
;; (require 'ffap-bugfix.el)
;;
;; an emacs report has been created for this (7229) and a fix might be included in the future.

(require 'advice)
(defadvice ffap-file-at-point (around bugfix-ffap  activate)
  "returns nil when point is somewhere we do not want to guess filenames"
  (require 'thingatpt)
;;; prevent in javadoc style comments
  (unless (or
           ;; ignore shebang lines
           (thing-at-point-looking-at "#!/[a-zA-Z0-9:_.-/]+")
           ;; ignore /**...* comment starts
           (thing-at-point-looking-at "/[\\*]+")
           ;; ignore // comments
           (thing-at-point-looking-at "//[a-zA-Z0-9:_.-/]*")
           ;; for xml modes
           (and (member major-mode '(sgml-mode nxml-mode))
                ;; don't care about <somenode/> and </somenode>
                (or (thing-at-point-looking-at "</?[a-zA-Z_][a-zA-Z0-9:_.-]*>?")
                    (thing-at-point-looking-at "/>"))))
    ad-do-it))

(provide 'ffap-bugfix)

;;; end of ffap-bugfix.el
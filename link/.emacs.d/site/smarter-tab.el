;;; smarter-tab.el --- tab key can indent whole regions if marked

;;; useful in particular in C, XML or python modes

(defun smart-indent ()
  "Indents region if mark is active, or current line otherwise."
  (interactive)
  (if mark-active
      (indent-region (region-beginning)
                     (region-end))
    (indent-for-tab-command)))

(defun smart-tab-indent ()
  "Indents region if mark is active, or current line otherwise. Minibuffer compliant."
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (smart-indent)))

(define-key c-mode-map (kbd "<tab>") 'smart-tab-indent)
(define-key c++-mode-map (kbd "<tab>") 'smart-tab-indent)

;; (require 'python)
;; (define-key inferior-python-mode-map (kbd "<tab>") 'smart-tab-indent)
;; (define-key python-mode-map (kbd "<tab>") 'smart-tab-indent)

;; for xml, html, etc
(add-hook 'sgml-mode-hook (lambda ()
                              (define-key sgml-mode-map (kbd "<tab>") 'smart-tab-indent)))

(provide 'smarter-tab)

;;; end of smarter-tab.el
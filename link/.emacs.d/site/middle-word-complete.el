;;; middle-word-complete.el --- complete word even when point is in middle of the word
;;
;; Copyright (C) 2010 Thibault Kruse
;;
;; Author: Thibault Kruse
;; Keywords: lisp,convenience
;; Version: 0.1
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;; 02111-1307, USA.
;;
;; Commentary:
;; This is inspired by smart-tab.el
;; It wraps completions in such a way that it works even when
;; point is in the middle of a word, but not by merely inserting guesses,
;; but in that case removing the rest of the word first.
;;
;; so
;; (de|fpack
;; M-x kill-rest-complete
;; may generate
;; (defun|
;; instead of
;; (defun|fpack
;; And if no completion exist, the buffer stays the same
;;
;; This is minibuffer compliant and does not do anything if a mark is active
;;
;;
;; It should work with dabbrev-expand and hippie-expand, though the hippie configuration might be crucial.
;; I would not use hippie unless with this hippie configuration:
;; '(hippie-expand-try-functions-list (quote (try-expand-dabbrev  try-complete-lisp-symbol-partially)))
;;
;; I use it with this in my emacs.d:
;; (require 'middle-word-complete)
;; (global-set-key (kbd "<C-tab>") 'middle-word-complete)
;;
;; Mixing this on the same key with smart-indent could be awkward, since point would have to be outside a word to not do completion

;; use e.g. 'hippie-expand or 'dabbrev-expand
(defvar *middle-word-complete-fun* 'hippie-expand)

(defun middle-word-complete (prefix)
  "calls hippie-expand, but may start in the middle of a word.
This smart tab is minibuffer compliant: it acts as usual in the minibuffer."
  (interactive "P")
  (if (minibufferp)
      (minibuffer-complete)
      ;; make sure completion is the right action, e.g. we are in or after a word
      (unless (or (consp prefix) ;; prefix is \\[universal-argument]
                  mark-active   ;; mark is active
                  (looking-at "^")) ;; beginning of line
        (if (looking-at "\\_>")
            ;; at end of word, do completion
            (funcall *middle-word-complete-fun* nil)
            ;; else in middle of word or on blank, check not on blank
            (when (looking-at "\\w")
              ;; kill the rest of word, try expanding
              (let* ((point-loc (point))
                     (end-loc (progn (forward-word 1) (point)))
                     (rest-word (buffer-substring point-loc end-loc))
                     (mod-p (buffer-modified-p)))
                (delete-region point-loc end-loc)
                (unless (funcall *middle-word-complete-fun* nil)
                  ;; if expanding fails, restore what had been killed
                  (insert rest-word)
                  ;; reset point where it was
                  (goto-char point-loc)
                  ;; reset dirty flag to virgin if it was
                  (unless mod-p
                    (set-buffer-modified-p nil)))))))))


(provide 'middle-word-complete)
;;; middle-word-complete ends here

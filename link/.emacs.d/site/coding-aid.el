;;; coding-aid.el --- folds away initial copyright note, enables textmate and autoindents python code
;;;
;;; Not everyone likes code folding with hs minor mode.
;;; I only like to use folding for the large copyright note on top of files.
;;; alternatively, you could try implementing jumping to the beginning of the file.
;;;
;;; this el file is not well suited for general import, since it does more than one thing.

;; (require 'hs-minor-mode) ;; now included in emacs 22 + 23
;; use with
;;(global-set-key "\C-ch" 'hs-hide-block)
;;(global-set-key "\C-cs" 'hs-show-block)
;;(global-set-key (kbd "<backtab>") 'hs-toggle-hiding)

;;; automatic second parentheses, also deletes both
(require 'textmate-mode)


(add-hook 'c-mode-hook (lambda ()
                          (textmate-mode)
                          (hs-minor-mode)
                          (let ((point (point)))
                            (goto-char 1)
                            (when (thing-at-point-looking-at "/")
                              (hs-hide-block))
                            (goto-char point))))

(add-hook 'c++-mode-hook (lambda ()
                            (textmate-mode)
                            (hs-minor-mode)
                            (let ((point (point)))
                              (goto-char 1)
                              (when (thing-at-point-looking-at "/")
                                (hs-hide-block))
                              (goto-char point))))

;;; bind RET to py-newline-and-indent
(add-hook 'python-mode-hook
          (lambda ()
            (textmate-mode)
            (define-key python-mode-map "\C-m" 'newline-and-indent)
            (hs-minor-mode)
            ;; fold copyright notice
            (let ((point (point)))
              (goto-char 1)
              (when (thing-at-point-looking-at "#")
                (hs-hide-block))
              (goto-char point))))

;; auto indent after hitting return, useful with Python mode
(defun my-make-CR-do-indent ()
  (define-key c-mode-base-map "\C-m" 'c-context-line-break))
(add-hook 'c-initialization-hook 'my-make-CR-do-indent)


(add-hook 'python-mode-hook 'my-make-CR-do-indent)

(add-hook 'lisp-mode-hook
          (lambda ()
            (hs-minor-mode)
            ;; fold copyright notice
            (let ((point (point)))
              (goto-char 1)
              (when (thing-at-point-looking-at ";")
                (hs-hide-block))
              (goto-char point))))

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (hs-minor-mode)
            ;; fold copyright notice
            (let ((point (point)))
              (goto-char 1)
              (when (thing-at-point-looking-at ";")
                (hs-hide-block))
              (goto-char point))))

(provide 'coding-aid)

;;; end of coding-aid.el
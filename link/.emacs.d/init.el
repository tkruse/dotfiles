;; no welcome screen

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

(setq inhibit-startup-message t)

;; Keystrokes to learn next
(defun fill-scratch()
  "add text to scratch buffer"
  (get-buffer-create "*scratch*")
  (with-current-buffer (get-buffer "*scratch*")
    (setq buffer-undo-list t)           ; Kein Undo aufnehmen.
    (insert "Key strokes to learn:\n")
    (insert "\n")
    (insert "<C-tab>                 simple completion\n")
    (insert "C-M-f, C-M-b            sexp forward\n")
    (insert "C-j                     newline indent\n")
    (insert "M-S-up,down,left-right  switch buffers\n")
    (insert "\n")
    (insert "M-q           pretty print  \n")
    (insert "C-x C-SPC     back globally\n")
    (insert "C-u C-SPC     back within buffer\n")
    (insert "C-c (, C-c )  barfage\n")
    (set-buffer-modified-p nil)))
;; fill scratch buffer with above
(add-hook 'window-setup-hook 'fill-scratch t)

;; solve problems with copy and paste (hopefully)
;;(setq x-select-enable-clipboard t)
;;(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)


;; the code below allows emacs to scroll slowly when using the mouse wheel, or when reaching the end of buffer with point
(defun smooth-scroll (increment)
  (scroll-up increment) (sit-for 0.05)
  (scroll-up increment) (sit-for 0.02)
  (scroll-up increment) (sit-for 0.02)
  (scroll-up increment) (sit-for 0.05)
  (scroll-up increment) (sit-for 0.06)
  (scroll-up increment))
(global-set-key [(mouse-5)] '(lambda () (interactive) (smooth-scroll 1)))
(global-set-key [(mouse-4)] '(lambda () (interactive) (smooth-scroll -1)))
(global-set-key [(triple-wheel-up)] '(lambda () (interactive) (smooth-scroll 1)))
(global-set-key [(triple-wheel-down)] '(lambda () (interactive) (smooth-scroll -1)))
(global-set-key [(double-wheel-up)] '(lambda () (interactive) (smooth-scroll 1)))
(global-set-key [(double-wheel-down)] '(lambda () (interactive) (smooth-scroll -1)))
(global-set-key [(wheel-up)] '(lambda () (interactive) (smooth-scroll 1)))
(global-set-key [(wheel-down)] '(lambda () (interactive) (smooth-scroll -1)))

;; smooth scrolling
(setq scroll-step 1)
(set-variable 'scroll-conservatively 10000)

(setq ring-bell-function 'ignore)

;; fix notebook keypad keys being useless with C
(global-set-key [C-kp-home]  'beginning-of-buffer) ; [Home]
(global-set-key [C-kp-end]   'end-of-buffer)       ; [End]

;; Public emacs site
;;(add-to-list 'load-path "/usr/share/emacs/site-lisp/emacs-color-themes")
(add-to-list 'load-path "~/.emacs.d/site")

;; C-x, C-c, C-v, C-z
(cua-mode t)
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour

;; custom patch to backspace
(require 'delete-region-backspace)
(global-set-key [backspace] 'tweakemacs-backward-delete-region-or-char)
;; make sure Isearch C-s is not disturbed
(define-key isearch-mode-map [backspace] 'isearch-delete-char)

;; need no toolbar nor menu
;(tool-bar-mode -1)
;(menu-bar-mode -1)

;; highlight matching parentheses
(require 'paren)
(show-paren-mode 1)

;; makes delete key perform kill-forward
(global-set-key '[delete] 'delete-char)

;; (condition-case nil
;;     (progn
;;       (add-to-list 'load-path "~/work/lisp/rosemacs")
;;       (require 'rosemacs)
;;       (invoke-rosemacs)

;;       (global-set-key "\C-x\C-r" ros-keymap)

;;       (require 'rng-loc)
;;       (push (concat (ros-package-path "rosemacs") "/rng-schemas.xml") rng-schema-locating-files))
;;   (error (message "error when loading rosemacs (check rospack)")))

(add-to-list 'auto-mode-alist '("\.launch$" . nxml-mode))
(add-to-list 'auto-mode-alist '("manifest.xml" . nxml-mode))

;;; enable multiple minibuffers:
(setq minibuffer-max-depth nil)

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 ;;'(bar-cursor-mode t nil (bar-cursor));; requires emacs-goodies-el installed
 '(c-basic-offset (quote set-from-style))
 '(column-number-mode t)
 '(cua-keep-region-after-copy nil)
 '(delete-selection-mode t)
 '(desktop-lazy-verbose nil)
 '(desktop-load-locked-desktop nil)
 '(desktop-path (quote ("~/.emacs.d")))
 '(desktop-restore-eager 1)
 '(desktop-save t)
 '(ecb-auto-activate t)
 '(ecb-compile-window-width (quote edit-window))
 '(ecb-layout-name "left3")
 '(ecb-layout-window-sizes (quote (("left3" (0.2235294117647059 . 0.28205128205128205) (0.2235294117647059 . 0.3333333333333333) (0.2235294117647059 . 0.358974358974359)))))
 '(ecb-options-version "2.32")
 '(ecb-primary-secondary-mouse-buttons (quote mouse-1--mouse-2))
 '(ecb-show-help-format (quote html))
 '(ecb-source-file-regexps (quote ((".*" ("\\(^\\(\\.\\|#\\)\\|\\(~$\\|\\.\\(elc\\|obj\\|o\\|class\\|lib\\|dll\\|a\\|so\\|cache\\|fasl64\\)$\\)\\)") ("^\\.\\(emacs\\|gnus\\)$")))))
 '(ecb-tip-of-the-day nil)
 '(ecb-tree-buffer-style (quote image))
 '(flyspell-abbrev-p t)
 '(flyspell-delay 2)
 '(flyspell-issue-welcome-flag nil)
 '(flyspell-large-region 10000)
 '(flyspell-use-global-abbrev-table-p t)
 '(flyspell-use-meta-tab nil)
 '(frame-background-mode (quote light))
 '(global-hl-line-mode nil nil (hl-line))
 '(gnuserv-program (concat exec-directory "/gnuserv"))
 '(hippie-expand-try-functions-list (quote (try-expand-dabbrev  try-complete-lisp-symbol-partially  yas/hippie-try-expand yas/hippie-try-prefix-expand)))
 '(ibuffer-default-shrink-to-minimum-size t)
 '(ibuffer-never-show-predicates (quote ("\\*ros[a-zA-Z\\D]*" "\\*slime-events\\*" "\\*slime-compilation\\*" "\\*Ibuffer\\*" "\\*inferior-lisp\\*")) nil (ibuf-ext))
 '(ibuffer-use-other-window t)
 '(ido-auto-merge-work-directories-length -1)
 '(ido-case-fold t)
 '(ido-confirm-unique-completion nil)
 '(ido-create-new-buffer (quote alway))
 '(ido-enable-flex-matching t)
 '(ido-everywhere t)
 '(ido-ignore-buffers (quote ("\\*ros[a-zA-Z\\D]*" "\\*slime-events\\*" "\\*slime-compilation\\*" "\\*Ibuffer\\*" "\\*inferior-lisp\\*" "\\` ")))
 '(ido-max-work-file-list 30)
 '(ido-mode (quote both) nil (ido))
 '(ido-save-directory-list-file "~/.emacs.d/.ido.last")
 '(ido-use-filename-at-point (quote guess))
 '(ido-use-url-at-point t)
 '(ispell-local-dictionary "american")
 '(line-number-mode t)
 '(load-home-init-file t t)
 '(mark-diary-entries-in-calendar t)
 '(mouse-wheel-follow-mouse t)
 '(mouse-wheel-scroll-amount (quote (2 . 1)))
 '(next-line-add-newlines nil)
 '(pabbrev-idle-timer-verbose nil)
 '(pabbrev-minimal-expansion-p t)
 '(pabbrev-read-only-error nil)
 '(pabbrev-scavenge-some-chunk-size 2000)
 '(paren-mode (quote paren) nil (paren))
 '(recentf-menu-filter (quote recentf-show-basenames))
 '(recentf-save-file "~/.emacs.d/.recentf")
 '(recentf-show-file-shortcuts-flag nil)
 '(ros-completion-function (quote ido-completing-read))
 '(safe-local-variable-values (quote ((Syntax . Common-Lisp) (Package . SAX) (Encoding . utf-8) (Syntax . COMMON-LISP) (Package . CL-PPCRE) (package . rune-dom) (readtable . runes) (Syntax . ANSI-Common-Lisp) (Base . 10))))
 '(savehist-mode t nil (savehist))
 '(scroll-bar-mode (quote right))
 '(transient-mark-mode t)
 '(use-dialog-box nil)
 '(view-diary-entries-initially t))

(autoload 'c++-mode  "cc-mode" "C++ Editing Mode" t)
(autoload 'c-mode    "cc-mode" "C Editing Mode"   t)
(autoload 'objc-mode "cc-mode" "Objective C Editing Mode" t)
(autoload 'text-mode "indented-text-mode" "Indented Text Editing Mode" t)
(autoload 'xrdb-mode "xrdb-mode" "Mode for editing X resource files" t)
(autoload 'ps-mode "ps-mode" "Major mode for editing PostScript" t)
(setq auto-mode-alist
  (append '(("\\.C$"       . c++-mode)
      ("\\.cc$"      . c++-mode)
      ("\\.c$"       . c-mode)
      ("\\.h$"       . c++-mode)
      ("\\.i$"       . c++-mode)
      ("\\.ii$"      . c++-mode)
      ("\\.m$"       . objc-mode)
      ("\\.pl$"      . perl-mode)
      ("\\.sql$"     . c-mode)
      ("\\.sh$"      . shell-script-mode)
      ("\\.mak$"     . makefile-mode)
      ("\\.GNU$"     . makefile-mode)
      ("makefile$"   . makefile-mode)
      ("Makefile$"   . makefile-mode)
      ("Imakefile$"  . makefile-mode)
      ("\\.Xdefaults$"    . xrdb-mode)
      ("\\.Xenvironment$" . xrdb-mode)
      ("\\.Xresources$"   . xrdb-mode)
      ("*.\\.ad$"         . xrdb-mode)
      ("\\.[eE]?[pP][sS]$" . ps-mode)
      ("\\.nsp"      . lisp-mode)
      ("\\.asd"      . lisp-mode)
      ("\\.vimpulse" . lisp-mode)
      ("\\.cl$"      . lisp-mode)
      ("\\.p3d$"      . conf-javaprop-mode)
      ("\\.macro$"      . conf-javaprop-mode)
      ("\\.pgf$" . latex-mode)
      ("\\.urdf$" . xml-mode)
      ("\\.org\\'" . org-mode)
      (".gitignore$"  . makefile-mode)
      (".gitconfig$"  . makefile-mode)
      ("config$"      . makefile-mode)
      ) auto-mode-alist))

(setq default-tab-width 2)
(setq tab-width 2)
(setq-default c-basic-offset 2)
(setq-default indent-tabs-mode nil)

(setq initial-major-mode 'text-mode)
(setq default-major-mode 'text-mode)
(global-font-lock-mode t)               ;colorize all buffers
(setq font-lock-maximum-decoration t)


; Search highlighting
; highlight during query
(setq query-replace-highlight t)
; highlight incremental search
(setq search-highlight t)


; C/C++ indentation config
(require 'cc-mode)
;; ffap extention looking up standard c
(require 'ffap-gcc-path)
;; allows ffap at beginning of include statement
(require 'ffap-include-start)
(require 'ffap-bugfix)



(setq c-basic-offset 2)
(setq c-default-style
      '((java-mode . "java") (other . "ellemtel")))
(add-hook 'java-mode-hook (lambda ()
                                (setq c-basic-offset 4
                                      tab-width 4
                                      indent-tabs-mode t)))
(add-hook 'groovy-mode-hook (lambda ()
                                (setq c-basic-offset 4
                                      tab-width 4
                                      indent-tabs-mode t)))
(setq c-offsets-alist '((arglist-cont-nonempty . +)))
(define-key c-mode-base-map "\C-c\C-c" 'recompile)


;; improved behavior for c, python, and lisp modes
(require 'coding-aid)
(global-set-key "\C-ch" 'hs-hide-block)
(global-set-key "\C-cs" 'hs-show-block)
(global-set-key (kbd "<backtab>") 'hs-toggle-hiding)

;; suppresses most annoying "active processes exist" question when killing emacs
(add-hook 'shell-mode-hook
          (lambda ()
            (process-kill-without-query
              (get-buffer-process (current-buffer)) nil)))


;;(define-key company-active-map (kbd "<tab>") 'company-complete)
;; (define-key company-active-map (kbd "\C-g") '(lambda ()
;;                                                (interactive)
;;                                                (company-abort)))


; paredit mode
;;(require 'paredit)
;;(add-hook 'emacs-lisp-mode-hook (lambda () (paredit-mode +1)))
;;(add-hook 'lisp-mode-hook (lambda () (paredit-mode +1)))
;;(add-hook 'inferior-lisp-mode-hook (lambda () (paredit-mode +1)))

;(autoload 'paredit-mode "paredit"
;  "Minor mode for pseudo-structurally editing Lisp code." t)
;(define-key paredit-mode-map (kbd "C-<right>") 'forward-word)
;(define-key paredit-mode-map (kbd "C-<left>") 'backward-word)
;(define-key paredit-mode-map (kbd "<delete>") 'delete-char)
;(define-key paredit-mode-map (kbd "C-c C-)") 'paredit-forward-barf-sexp)
;(define-key paredit-mode-map (kbd "C-c C-(") 'paredit-backward-barf-sexp)


;; something similar to "mark occurences" in eclipse, but less smart
(load "light-symbol-mode.el")
(add-hook 'emacs-lisp-mode-hook (lambda () (light-symbol-mode)))
(add-hook 'lisp-mode-hook (lambda () (light-symbol-mode)))
(add-hook 'python-mode-hook (lambda () (light-symbol-mode)))
(add-hook 'shell-script-mode-hook (lambda () (light-symbol-mode)))
(add-hook 'c-mode-hook (lambda () (light-symbol-mode)))
(add-hook 'c++-mode-hook (lambda () (light-symbol-mode)))
(autoload 'light-symbol-mode "light symbol mode"
  "highlight code."
  t)



; Mouse wheel
; not for emacs 22 (autoload 'mwheel-install "mwheel" "Enable mouse wheel support.") (mwheel-install)


; Ignore .svn stuff in grep-find
(setq grep-find-command "find . -type f -not -name \"*.svn-base\" -and -not -name \"*.tmp\" -print0 | xargs -0 -e grep -i -n -s -F ")

;; spell-check
(setq ispell-program-name "aspell")
(add-hook `latex-mode-hook `flyspell-mode)
(add-hook `LaTeX-mode-hook `flyspell-mode)
(add-hook `tex-mode-hook `flyspell-mode)
(add-hook `bibtex-mode-hook `flyspell-mode)

;; (add-hook `flyspell-mode-hook '
;;           (define-key flyspell-mode-map [(mouse-2)] nil)

;;           ;; so as to not interfere with mouse-drag
;;           (define-key flyspell-mouse-map [(mouse-2)] nil)
;;           (define-key flyspell-mouse-map [(down-mouse-2)] 'flyspell-correct-word))


;;Load auctex (obsolete, already in emacs by Linux)
;; (load "auctex")
;; (load "preview-latex")
;; (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
;; (add-hook 'LaTeX-mode-hook 'pabbrev-mode)
;; (add-hook 'LaTeX-mode-hook 'reftex-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

; Load cool git frontend
;(require 'magit)

;(require 'pabbrev)

; all buffers have tab like buttons showing other buffers
;(require 'tabbar)
;(tabbar-mode)

; will allow you to type just "y" instead of "yes" when you exit.
(fset 'yes-or-no-p 'y-or-n-p)

; make parens a different color
;(require 'paren-face)

; use firefox
(setq browse-url-browser-function 'browse-url-firefox)


;; change font size
;; (set-face-attribute 'default nil :height 160)

;; hl-line: highlight the current line
(when (fboundp 'global-hl-line-mode)
  (global-hl-line-mode t)) ;; turn it on for all modes by default


;; restore window configuration
(require 'winner)
(when (fboundp 'winner-mode)
  (winner-mode 1))

;; use super + arrow keys to switch between visible buffers
(require 'windmove)
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings 'meta) ;using windows key
  ;;  (windmove-default-keybindings)
  )

;; extention to windmove, allows switching buffers
(require 'buffer-move)
(global-set-key (kbd "<M-S-up>")     'buf-move-up)
(global-set-key (kbd "<M-S-down>")   'buf-move-down)
(global-set-key (kbd "<M-S-left>")   'buf-move-left)
(global-set-key (kbd "<M-S-right>")  'buf-move-right)

;;start emacs server for many sessions
;;(server-start)


;; allows looking at a least of recently used files and opening from that list
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 100)
(setq recentf-exclude (append recentf-exclude '(".ftp:.*" ".sudo:.*")))
(setq recentf-keep '(file-remote-p file-readable-p))

;;Use electric help, puts cursor into help buffer
(require 'ehelp)
(define-key global-map "\C-h" 'ehelp-command)

(condition-case nil
    (progn
      (require 'yaml-mode)
      (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode)))
  (error (message "error when loading yaml")))

;; ibuffer mode that is better than electric buffer
(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)

(setq ropemacs-confirm-saving nil
      ropemacs-guess-project t
      ropemacs-enable-autoimport t)

(condition-case nil
    (progn
      (require 'smex)
      ;;smex replaces M-x with IDO enhancements
      (setq smex-save-file "~/.emacs.d/smex.save")

      (smex-initialize)
      (global-set-key (kbd "M-x") 'smex)
      (global-set-key (kbd "M-X") 'smex-major-mode-commands)
      (global-set-key (kbd "C-c M-x") 'smex-update-and-run)
      ;; This is your old M-x.
      (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command))
  (error (message "error when loading smex")))

;; tab indents region if region is shift marked
(require 'smarter-tab)

;; C-<tab> hippie-expends even when in the middle of word
(require 'middle-word-complete)
(global-set-key (kbd "<C-tab>") 'middle-word-complete)


;; (x-focus-frame nil)

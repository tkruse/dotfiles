# Maven
export M2_REPO="${HOME}/.m2/repository"
export MAVEN_OPTS="-Xms256m -Xmx512m"

if [ -d "$M2_HOME" ]; then
  M2_JARS=$M2_HOME/lib/*.jar
  for f in $M2_JARS
  do
    M2_CLASSPATH=$M2_CLASSPATH:$f
  done
  export M2_CLASSPATH
fi

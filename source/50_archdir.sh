#!/bin/bash

# establish install location in ~/local all and ~/local/architecture

case ${MACHTYPE} in
  "i386-pc-linux-gnu" | "i486-pc-linux-gnu")
    export ARCHITECTURE=i486-unknown-linux2.0.0
    export ARCH_SHORT=i386;;
  "x86_64-pc-linux-gnu" | "x86_64")
    export ARCHITECTURE=amd64-unknown-linux2.x
    export ARCH_SHORT=amd64;;
  "i686-pc-linux-gnu")
    export ARCHITECTURE=i686-unknown-linux2.x
    export ARCH_SHORT=i686;;
  "x86_64-apple-darwin17.5.0")
    export ARCHITECTURE=i686
    export ARCH_SHORT=darwin;;
  "x86_64-apple-darwin18.7.0")
    export ARCHITECTURE=i686
    export ARCH_SHORT=darwin;;
  "x86_64-apple-darwin17.7.0")
    export ARCHITECTURE=i686
    export ARCH_SHORT=darwin;;
  "x86_64-apple-darwin21.1.0")
    export ARCHITECTURE=i686
    export ARCH_SHORT=darwin;;
  *)
    export ARCHITECTURE=unknown
    export ARCH_SHORT=unknown
    echo "dotfile_prompt: unknown MACHTYPE=${MACHTYPE}"
esac



# architecture directory
export ARCH_DIR=${ARCH_SHORT}${ARCH_SUFFIX:+-$ARCH_SUFFIX}
export ARCH_PATH=${HOME}/local/${ARCH_DIR}

# private path
export PATH=${ARCH_PATH}/bin:${ARCH_PATH}/bin:${HOME}/local/all/bin:${PATH}


export LD_LIBRARY_PATH=${HOME}/local/all/lib:${ARCH_PATH}/lib:${LD_LIBRARY_PATH}
export MANPATH=${HOME}/local/all/man:${ARCH_PATH}/man:${MANPATH}
# export PYTHONPATH=${ARCH_PATH}/lib/${PYTHON_VERSION}/site-packages
export PKG_CONFIG_PATH=${HOME}/local/all/lib/pkgconfig:${ARCH_PATH}/lib/pkgconfig:${ARCH_PATH}/share/pkgconfig
export CPATH=${ARCH_PATH}/include
export LDFLAGS="-L${HOME}/local/all/lib -L${ARCH_PATH}/lib ${LDFLAGS}"
export INFOPATH=$INFOPATH:${ARCH_PATH}/share/info:${HOME}/local/all/share/info

export CMAKE_PREFIX_PATH=${ARCH_PATH}/

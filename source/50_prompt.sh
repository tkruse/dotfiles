#!/bin/bash


 # If not running interactively (e.g. over git or scripts), don't do anything
[ -z "$PS1" ] && return

#####################################################
# execution returns after this line in non-interactive
#####################################################


# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes




# prompt
export PS1="${debian_chroot:+$debian_chroot-}${ARCH_SUFFIX:+$ARCH_SUFFIX-}\u@\h[\w]$ "
export PS2='>'
export PS4='+'

export PROMPT_COMMAND='( x=$? ; let x!=0 && echo !!! BASH reports ERROR: shell returned $x )'

if [ -n "$PS1" ]; then
  # modifying the prompt command to set temporary variables, using ^ to represent ~ for now, replaced later
  # using git and sed commands to find out branch name, reducing it to 15 chars and removing brackets if empty
    NEW_PROMPT_COMMAND='
    TRIMMED_SUBST=${PWD/$HOME/'^'};
    TRIMMED_PWD=${TRIMMED_SUBST: -50};
    TRIMMED_PWD=${TRIMMED_PWD:-$TRIMMED_SUBST}
    TRIMMED_PWD_S=${TRIMMED_SUBST: -15};
    TRIMMED_PWD_S=${TRIMMED_PWD_S:-$TRIMMED_SUBST}
    GITREF=$(parse_git_branch)
    TRIMMED_GITREF=${GITREF:0:15};
    TRIMMED_GITREF="(${TRIMMED_GITREF:-$GITREF})"
    TRIMMED_GITREF=${TRIMMED_GITREF/()/}
    HGREF=$(parse_hg_branch)
    HGREF=$(trim $HGREF)
    HGBREF=$(parse_hg_bookmark)
    HGBREF=$(trim $HGBREF)
    HGREF="$HGREF $HGBREF"
    HGREF=$(trim $HGREF)
    TRIMMED_HGREF=${HGREF:0:15};
    TRIMMED_HGREF="[${TRIMMED_HGREF:-$HGREF}]"
    TRIMMED_HGREF=${TRIMMED_HGREF/[]/}
    '

    # If there's an existing prompt command, append
    if [ -n "$PROMPT_COMMAND" ]; then
        PROMPT_COMMAND="$PROMPT_COMMAND;$NEW_PROMPT_COMMAND"
    else
        PROMPT_COMMAND="$NEW_PROMPT_COMMAND"
    fi

  trim() { echo $1 $2; }

  parse_git_branch ()
  {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
  }

  parse_hg_bookmark ()
  {
      hg bookmarks  2> /dev/null | sed -e '/^  /d' -e '/^no/d' -e 's/* \([a-zA-Z0-9]*\).*/\1/'
  }

  parse_hg_branch ()
  {
    hg summary 2> /dev/null | sed -e '/^[^parent]/d' -e 's/parent: \([0-9]*\):[a-zA-Z0-9]*\(.\)\(.*\)/\1\2\3/'
  }

 # We're done with our temporary variable
  unset NEW_PROMPT_COMMAND

  export PS1="   ${debian_chroot:+($debian_chroot)}\[\033[01;35m\]@ \h\[\033[00m\] : \
\[\033[01;35m\]"'${TRIMMED_PWD/^/'~'}'"\
\[\033[00m\] \[\033[31m\]"'$TRIMMED_GITREF'"\[\033[00m\]\
\[\033[00m\] \[\033[31m\]"'$TRIMMED_HGREF'"\[\033[00m\]\n\
\[\033[04;32m\]\u\[\033[00m\]:\[\033[01;34m\]"'${TRIMMED_PWD_S/^/'~'}'"\[\033[00m\] \$ "
fi
# ls
export LS_COLORS='no=00:fi=00:di=01;34:ln=01;35:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jpg=01;35:*.gif=01;35:*.bmp=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.png=01;35:*.mpg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:';
# bash
export HISTCONTROL=ignoreboth
# grep
export GREP_COLOR='0;31'
# rsync
export RSYNC_RSH=/usr/bin/ssh


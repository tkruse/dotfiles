# .alias

alias rm='rm'
alias ls='ls -F --color=auto'
alias ll='ls -l'
alias lll='ls -l --color=always | more'
alias la='ls -al'
alias lal='ls -al --color=always | more'
alias lr='ls -R'
alias llr='ls -lR'
alias lar='ls -alR'
alias dir='ls -l | grep "^d"'
alias cgrep='grep -n --color=always'
alias cdp='cd -P `pwd`'

alias pu='pushd'
alias po='popd'

alias afind='ack-grep -il'

alias pp='sed s/:/\\n/g'

alias less="less -I -R"


# my aliases ;)
alias tig-st='tig status'

alias tm='tmux attach || tmux new'

alias nano='nano -w -S'

alias ipython='ipython --nosep --no-confirm-exit'

alias ag='find . ! -regex ".*[/]\..+[/]?.*" -print0 | xargs -0 -n1000 grep -IHn'

#alias allegro='pipe-rlwrap --fifo=${HOME}/.lisp-pipe -b ${LISP_BREAK_CHARS} allegro-lisp -I ${ACLCORE}'
#alias vi='vim --servername VIM'

alias ec="emacsclient -n -e  '(new-frame)' -a emacs"
alias git-graph='git log --graph --pretty=oneline --decorate --all'
alias 'ps?'='ps ax | grep '

alias gitkall='gitk --all'
alias gitkref='gitk --all $(git log -g --pretty=format:%h)'
alias gg='git gui'

alias grep='grep --color=auto --exclude-dir=.svn --exclude=\*.pyc --exclude=\*.so --exclude=\*.class --exclude=\*.fasl\* --exclude-dir=.hg --exclude-dir=.bzr --exclude-dir=.git'
alias rgrep='rgrep --color=auto'
alias fgrep='fgrep --color=auto'

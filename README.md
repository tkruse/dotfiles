# Dotfiles

( #movingtogitlab )

Dotfiles initializes a system and copies / symlinks dotfiles.

On Ubuntu, requires:

    apt-get update
    apt-get install git-core sudo lsb-release software-properties-common dialog

Run install directly from github (clones itself):

    git clone https://gitlab.com/tkruse/dotfiles .dotfiles
    cd .dotfiles
    bin/dotfiles
    source ~/.bashrc


# Things I changed after forking

* removed submodules
* removed plenty of stuff I do not need. And I mean *plenty*.
* changed my credentials in link/.gitconfig
* changed default _GH_ variables in bin/dotfiles
* added my personal files and packages

# TODO:

* Restore installation of node, use nvm
* install sdkman


* Bloat in Ubuntu Wily

sudo apt-get purge account-plugin-aim account-plugin-facebook account-plugin-flickr account-plugin-jabber account-plugin-salut account-plugin-twitter account-plugin-windows-live account-plugin-yahoo aisleriot brltty colord deja-dup deja-dup-backend-gvfs duplicity empathy empathy-common evolution-data-server-online-accounts example-content  gnome-accessibility-themes gnome-contacts gnome-mahjongg gnome-mines gnome-orca gnome-screensaver gnome-sudoku gnome-video-effects gnomine landscape-common libsane libsane-common mcp-account-manager-uoa python3-uno rhythmbox rhythmbox-plugins rhythmbox-plugin-zeitgeist sane-utils shotwell shotwell-common telepathy-gabble telepathy-haze telepathy-idle telepathy-indicator telepathy-logger telepathy-mission-control-5 telepathy-salut thunderbird thunderbird-gnome-support totem totem-common totem-plugins unity-scope-audacious unity-scope-chromiumbookmarks unity-scope-clementine unity-scope-colourlovers unity-scope-devhelp unity-scope-firefoxbookmarks unity-scope-gdrive unity-scope-gmusicbrowser unity-scope-gourmet unity-scope-manpages unity-scope-musicstores unity-scope-musique unity-scope-openclipart unity-scope-texdoc unity-scope-tomboy unity-scope-video-remote unity-scope-virtualbox unity-scope-yelp unity-scope-zotero


## License
Copyright (c) 2014 "Cowboy" Ben Alman  
Licensed under the MIT license.  
<http://benalman.com/about/license/>
